
using the NodeJS package python-shell, NodeJS can
make python calls into pyeq2 for curve fitting and
surface fitting.  I installed python-shell using npm.
see http://commonproblems.readthedocs.io/en/latest/

First install pyeq2 with the command

pip install pyeq2


then run the nodejs-to-pyeq2 interface generator with

python generator.py


and finally run the NodeJS example files:

LinearPolynomial_2D.js
ExponentialWithOffset_2D.js

LinearPolynomial_3D.js
