from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import os, sys, inspect

import pyeq2


for submodule in inspect.getmembers(pyeq2.Models_2D):
    if inspect.ismodule(submodule[1]):
        for equationClass in inspect.getmembers(submodule[1]):
            if inspect.isclass(equationClass[1]):
                # the 3D version demonstrates exclusion by equation attribute
                for extendedVersionName in pyeq2.ExtendedVersionHandlers.extendedVersionHandlerNameList:
                    try:
                        equation = equationClass[1]('SSQABS', extendedVersionName)
                    except:
                        continue
                    print('2D ' + submodule[0] + ' --- ' + equation.GetDisplayName())
                    
print('Done.')
