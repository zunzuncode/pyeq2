from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import os, sys, inspect

import pyeq2


# parameters are smoothing, xOrder, yOrder
equation = pyeq2.Models_3D.Spline.Spline(1.0, 3, 3) # cubic 3D spline

data = equation.exampleData

pyeq2.dataConvertorService().ConvertAndSortColumnarASCII(data, equation, False)
equation.Solve()


##########################################################


print("Equation:", equation.GetDisplayName(), str(equation.GetDimensionality()) + "D")
print("Fitting target of", equation.fittingTargetDictionary[equation.fittingTarget], '=', equation.CalculateAllDataFittingTarget(equation.solvedCoefficients))

print()

# at present, only these four languages are supported for spline-specific code
print(pyeq2.outputSourceCodeService().GetOutputSourceCodeCPP(equation))
#print(pyeq2.outputSourceCodeService().GetOutputSourceCodePYTHON(equation))
#print(pyeq2.outputSourceCodeService().GetOutputSourceCodeJAVA(equation))
#print(pyeq2.outputSourceCodeService().GetOutputSourceCodeJAVASCRIPT(equation))
